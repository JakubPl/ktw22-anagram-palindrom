import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        /*
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz slowo do sprawdzenia");
        String wordToCheck = scanner.nextLine();
        checkIsPalindrom(wordToCheck);
        */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz 2 slowa do sprawdzenia");
        String maybeAnagramWord1 = scanner.nextLine();
        String maybeAnagramWord2 = scanner.nextLine();
        if(checkIsAnagram(maybeAnagramWord1, maybeAnagramWord2)) {
            System.out.println("To anagramy");
        } else {
            System.out.println("To nie anagramy");
        }
    }

    private static boolean checkIsAnagram(String word1, String word2) {
        char[] word1Characters = word1.toCharArray();
        char[] word2Characters = word2.toCharArray();
        Arrays.sort(word1Characters);
        Arrays.sort(word2Characters);
        return Arrays.equals(word1Characters, word2Characters);
    }

    private static void checkIsPalindrom(String wordToCheck) {
        int a = 0;
        int z = wordToCheck.length() - 1;
        // wordToCheck.charAt(1); - zwraca litere na pozycji 1
        // wordToCheck.charAt(0); - zwraca litere na pozycji 0
        boolean isPalindrom = true;
        while (a <= z) {
            if (wordToCheck.charAt(a) != wordToCheck.charAt(z)) {
                isPalindrom = false;
                break;
            }
            a++;
            z--;
        }
        if (isPalindrom) {
            System.out.println("Jest palindromem");
        } else {
            System.out.println("To nie jest palindromem");
        }
    }
}
